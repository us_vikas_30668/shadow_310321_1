package com.example.officerecords.utilities;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {

    private static final Object LOCK = new Object();
    private static AppExecutors sInstance;
    private final Executor diskIO;
    private final Executor networkIO;
    private final Executor mainThread;
    private static final int N_THREADS = 3;

    public AppExecutors(Executor diskIO, Executor networkIO, Executor mainThread) {
        this.diskIO = diskIO;
        this.networkIO = networkIO;
        this.mainThread = mainThread;
    }

    // singleton design pattern
    public static AppExecutors getInstance(){
        if(sInstance == null){
            synchronized (LOCK){
                sInstance = new AppExecutors(
                        // diskIO
                        Executors.newSingleThreadExecutor(),
                        // networkIO
                        Executors.newFixedThreadPool(N_THREADS),
                        // Main Thread
                        new MainThreadExecutor()
                );
            }
        }
        return sInstance;
    }

    // main thread executor
    private static class MainThreadExecutor implements Executor{
        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable runnable) {
            mainThreadHandler.post(runnable);
        }
    }

    //
    public Executor getDiskIO(){
        return diskIO;
    }

    //
    public Executor getNetworkIO(){
        return networkIO;
    }

    //
    public Executor getMainThread(){
        return mainThread;
    }
}
