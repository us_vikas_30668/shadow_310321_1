package com.example.officerecords.utilities;

public class Data {

    public static final String EDITOR_EXTRA_ID = "com.example.officerecords.Editor.EXTRA_ID";
    public static final String EDITOR_EXTRA_NAME = "com.example.officerecords.Editor.EXTRA_NAME";
    public static final String EDITOR_EXTRA_EMAIL = "com.example.officerecords.Editor.EXTRA_EMAIL";
    public static final String EDITOR_EXTRA_DESIGNATION = "com.example.officerecords.Editor.EXTRA_DESIGNATION";
    public static final String EDITOR_EXTRA_REPORT_SUBMITTED = "com.example.officerecords.Editor.EXTRA_REPORT_SUBMITTED";

    public static final int ADD_EMPLOYEE_REQUEST_CODE = 1;
    public static final int EDIT_EMPLOYEE_REQUEST_CODE = 2;
}
