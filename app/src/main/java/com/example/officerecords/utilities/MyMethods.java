package com.example.officerecords.utilities;

import androidx.core.content.ContextCompat;

import com.example.officerecords.R;

public class MyMethods {

    public static int getStatusColorId(boolean val){

        int color_id = R.color.colorRed;
        if(val){
            color_id = R.color.colorGreen;
        }
        return color_id;
    }
}
