package com.example.officerecords.view.rv;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.officerecords.model.Employee;

public class EmployeeListAdapter extends ListAdapter<Employee, EmployeeViewHolder> {

    private OnItemClickListener listener;

    public EmployeeListAdapter() {
        super(new EmployeeDiff());
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return EmployeeViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, final int position) {
        Employee employee = getItem(position);
        holder.bind(employee);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null && position != RecyclerView.NO_POSITION)
                    listener.OnItemClick(getItem(position));
            }
        });
    }

    public static class EmployeeDiff extends DiffUtil.ItemCallback<Employee> {

        @Override
        public boolean areItemsTheSame(@NonNull Employee oldItem, @NonNull Employee newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Employee oldItem, @NonNull Employee newItem) {
            return oldItem.getEmail().equals(newItem.getEmail());
        }
    }

    // For Edit an Employee
    public interface OnItemClickListener{
        public void OnItemClick(Employee employee);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    // get employee item at position
    public Employee getEmployeeAt(int position){
        return getItem(position);
    }
}
