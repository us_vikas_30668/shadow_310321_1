package com.example.officerecords.view.rv;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.officerecords.R;
import com.example.officerecords.model.Employee;
import com.example.officerecords.utilities.MyMethods;

import butterknife.ButterKnife;

public class EmployeeViewHolder extends RecyclerView.ViewHolder {

    private final TextView text_view_status;
    private final TextView text_view_name;
    private final TextView text_view_email;
    private GradientDrawable status_circle;
    private final Context context;

    // constructor
    private EmployeeViewHolder(@NonNull final View itemView) {
        super(itemView);
        text_view_status = itemView.findViewById(R.id.text_view_status);
        text_view_name = itemView.findViewById(R.id.text_view_name);
        text_view_email = itemView.findViewById(R.id.text_view_email);
        status_circle = (GradientDrawable) text_view_status.getBackground();
        context = itemView.getContext();
    }

    // bind data with the view
    public void bind(Employee employee){
        text_view_name.setText(employee.getName());
        text_view_email.setText(employee.getEmail());

        // get status color
        int color_id = MyMethods.getStatusColorId(employee.isReportSubmitted());
        int color_code = ContextCompat.getColor(context, color_id);
        status_circle.setColor(color_code);
    }

    //
    static EmployeeViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new EmployeeViewHolder(view);
    }
}
