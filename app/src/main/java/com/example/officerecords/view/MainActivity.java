package com.example.officerecords.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.officerecords.view.rv.EmployeeListAdapter;
import com.example.officerecords.viewmodel.EmployeeViewModel;
import com.example.officerecords.R;
import com.example.officerecords.model.Employee;
import com.example.officerecords.utilities.Data;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EmployeeViewModel employeeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Floating Action Button (add)
        FloatingActionButton floating_button_add = findViewById(R.id.action_button_add);
        floating_button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                startActivityForResult(intent, Data.ADD_EMPLOYEE_REQUEST_CODE);
            }
        });

        // Recycler View & adapter
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        final EmployeeListAdapter adapter = new EmployeeListAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // view model persists even if the Activity is destroyed
        employeeViewModel = new ViewModelProvider(this).get(EmployeeViewModel.class);

        employeeViewModel.getAllRecords().observe(this, new Observer<List<Employee>>() {
            @Override
            public void onChanged(List<Employee> employees) {
                adapter.submitList(employees);
            }
        });

        // Item Touch
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                Employee employee = adapter.getEmployeeAt(position);

                boolean reportSubmitted = employee.isReportSubmitted();
                employee.setReportSubmitted(!reportSubmitted);
                employeeViewModel.updateEmployee(employee);
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setOnItemClickListener(new EmployeeListAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(Employee employee) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                intent.putExtra(Data.EDITOR_EXTRA_ID, employee.getId());
                intent.putExtra(Data.EDITOR_EXTRA_NAME, employee.getName());
                intent.putExtra(Data.EDITOR_EXTRA_EMAIL, employee.getEmail());
                intent.putExtra(Data.EDITOR_EXTRA_DESIGNATION, employee.getDesignation());
                intent.putExtra(Data.EDITOR_EXTRA_REPORT_SUBMITTED, employee.isReportSubmitted());

                startActivityForResult(intent, Data.EDIT_EMPLOYEE_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Data.ADD_EMPLOYEE_REQUEST_CODE && resultCode == RESULT_OK) {
            String name = data.getStringExtra(Data.EDITOR_EXTRA_NAME);
            String email = data.getStringExtra(Data.EDITOR_EXTRA_EMAIL);
            String designation = data.getStringExtra(Data.EDITOR_EXTRA_DESIGNATION);
            boolean reportSubmitted = data.getBooleanExtra(Data.EDITOR_EXTRA_REPORT_SUBMITTED, false);

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(designation)) {

            } else {
                Employee employee = new Employee(name, email, designation, reportSubmitted);
                employeeViewModel.insertEmployee(employee);

                Toast.makeText(this, "Employee saved", Toast.LENGTH_SHORT);
            }
        } else if (requestCode == Data.EDIT_EMPLOYEE_REQUEST_CODE && resultCode == RESULT_OK) {

            int id = data.getIntExtra(Data.EDITOR_EXTRA_ID, -1);
            if (id == -1) {
                Toast.makeText(this, "Record can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }
            String name = data.getStringExtra(Data.EDITOR_EXTRA_NAME);
            String email = data.getStringExtra(Data.EDITOR_EXTRA_EMAIL);
            String designation = data.getStringExtra(Data.EDITOR_EXTRA_DESIGNATION);
            boolean reportSubmitted = data.getBooleanExtra(Data.EDITOR_EXTRA_REPORT_SUBMITTED, false);

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(designation)) {

            } else {
                Employee employee = new Employee(name, email, designation, reportSubmitted);
                employee.setId(id);
                employeeViewModel.updateEmployee(employee);

                Toast.makeText(this, "Employee updated", Toast.LENGTH_SHORT);
            }

        } else {
            Toast.makeText(this, "Employee not saved", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_cold_boot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.cold_boot:
                employeeViewModel.updateAllEmployees();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
