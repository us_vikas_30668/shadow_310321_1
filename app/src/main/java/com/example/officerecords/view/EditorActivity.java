package com.example.officerecords.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.officerecords.R;
import com.example.officerecords.utilities.Data;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditorActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_name)
    EditText edit_text_name;
    @BindView(R.id.edit_text_email)
    EditText edit_text_email;
    @BindView(R.id.edit_text_designation)
    EditText edit_text_designation;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    private RadioButton radio_reportSubmitted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if(intent.hasExtra(Data.EDITOR_EXTRA_ID)){
            setTitle("EDIT EMPLOYEE RECORD");
            edit_text_name.setText(intent.getStringExtra(Data.EDITOR_EXTRA_NAME));
            edit_text_email.setText(intent.getStringExtra(Data.EDITOR_EXTRA_EMAIL));
            edit_text_designation.setText(intent.getStringExtra(Data.EDITOR_EXTRA_DESIGNATION));

            RadioButton radioYes = findViewById(R.id.radioYes);
            RadioButton radioNo = findViewById(R.id.radioNo);
            boolean reportSubmitted = intent.getBooleanExtra(Data.EDITOR_EXTRA_REPORT_SUBMITTED, false);
            if(reportSubmitted){
                radioYes.setChecked(true);
            }else{
                radioNo.setChecked(true);
            }

        }else{
            setTitle("Add Employee");
        }

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

    }

    // Save Employee to the Database
    private void saveEmployee(){
        String name = edit_text_name.getText().toString().trim();
        String email = edit_text_email.getText().toString().trim();
        String designation = edit_text_designation.getText().toString().trim();
        int selectedId = radioGroup.getCheckedRadioButtonId();
        boolean reportSubmitted = false;
        if(selectedId != -1){
            reportSubmitted = selectedId == R.id.radioYes;
        }else{
            Toast.makeText(getApplicationContext(), getString(R.string.mark_report_submitted), Toast.LENGTH_SHORT).show();
        }
        Intent data = new Intent();
        Log.i("EDITOR_ACTIVITY", "Hi " + name);
        data.putExtra(Data.EDITOR_EXTRA_NAME, name);
        data.putExtra(Data.EDITOR_EXTRA_EMAIL, email);
        data.putExtra(Data.EDITOR_EXTRA_DESIGNATION, designation);
        data.putExtra(Data.EDITOR_EXTRA_REPORT_SUBMITTED, reportSubmitted);

        int id = getIntent().getIntExtra(Data.EDITOR_EXTRA_ID, -1);
        if(id != -1){
            data.putExtra(Data.EDITOR_EXTRA_ID, id);
        }
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_employee:
                Log.i("EDITOR_ACTIVITY", "Save employee clicked");
                saveEmployee();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}