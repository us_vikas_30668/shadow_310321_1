package com.example.officerecords.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

/**
 * Schema for the table "employee_table"
 * id, name, email, designation
 */
@Entity(tableName = "employee_table", indices = {@Index(value = "email", unique = true)})
public class Employee {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String email;
    private String designation;
    private boolean reportSubmitted = false;

    /**
     * Constructor
     * @param name name of the employee
     * @param email email
     * @param designation designation
     */
    public Employee(String name, String email, String designation, boolean reportSubmitted) {
        this.name = name;
        this.email = email;
        this.designation = designation;
        this.reportSubmitted = reportSubmitted;
    }

    /**
     * Set Id
     * @param id id of the record
     */
    public void setId(int id){
        this.id = id;
    }

    /**
     *
     * @param reportSubmitted
     */
    public void setReportSubmitted(boolean reportSubmitted){
        this.reportSubmitted = reportSubmitted;
    }

    /**
     * Get id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Get designation
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Report submitted or not
     * @return reportSubmitted
     */
    public boolean isReportSubmitted() {
        return reportSubmitted;
    }
}
