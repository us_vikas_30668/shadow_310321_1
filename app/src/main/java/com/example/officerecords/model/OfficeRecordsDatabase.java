package com.example.officerecords.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Employee.class}, version = 1, exportSchema = false)
public abstract class OfficeRecordsDatabase extends RoomDatabase {

    private static OfficeRecordsDatabase instance;

    // Room self creates implementation of this method
    public abstract EmployeeDAO employeeDAO();

    /**
     * get DB instance
     * synchronized: only 1 thread can access it at a time so that you don't create 2+ instances of the DB
     */
    public static synchronized OfficeRecordsDatabase getInstance(Context context){

        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), OfficeRecordsDatabase.class, "office_records")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
