package com.example.officerecords.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.officerecords.model.Employee;

import java.util.List;

@Dao
public interface EmployeeDAO {

    @Insert
    void insert(Employee e);

    @Update
    void update(Employee e);

    @Delete
    void delete(Employee e);

    @Query("DELETE FROM employee_table")
    void deleteAll();

    @Query("SELECT * FROM employee_table ORDER BY name ASC")
    LiveData<List<Employee>> getAll();

    @Query("UPDATE employee_table SET reportSubmitted=0")
    void updateAll();
}
