package com.example.officerecords.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.officerecords.utilities.AppExecutors;

import java.util.List;

public class EmployeeRepository {

    private EmployeeDAO employeeDAO;
    private LiveData<List<Employee>> allRecords;

    /**
     * get database instance, dao
     * @param application
     */
    public EmployeeRepository(Application application){

        OfficeRecordsDatabase officeRecordsDatabase = OfficeRecordsDatabase.getInstance(application);
        employeeDAO = officeRecordsDatabase.employeeDAO();
        allRecords = employeeDAO.getAll();
    }

    /**
     * Insert a new employee
     * @param e
     */
    public void insertEmployee(final Employee e){
//        new InsertEmployee(employeeDAO).execute(e);
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                employeeDAO.insert(e);
            }
        });
    }
//    private static class InsertEmployee extends AsyncTask<Employee, Void, Void>{
//
//        private EmployeeDAO employeeDAO;
//
//        public InsertEmployee(EmployeeDAO employeeDAO) {
//            this.employeeDAO = employeeDAO;
//        }
//
//        @Override
//        protected Void doInBackground(Employee... employees) {
//            employeeDAO.insert(employees[0]);
//            return null;
//        }
//    }

    /**
     * Update an employee using AsyncTask
     * @param e
     */
    public void updateEmployee(final Employee e){
//        new UpdateEmployee(employeeDAO).execute(e);
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                employeeDAO.update(e);
            }
        });
    }
//    private static class UpdateEmployee extends AsyncTask<Employee, Void, Void>{
//
//        private EmployeeDAO employeeDAO;
//
//        public UpdateEmployee(EmployeeDAO employeeDAO) {
//            this.employeeDAO = employeeDAO;
//        }
//
//        @Override
//        protected Void doInBackground(Employee... employees) {
//            employeeDAO.update(employees[0]);
//            return null;
//        }
//    }

    /**
     * Update all employees
     * SET reportSubmitted = false
     */
    public void updateEmployees(){
//        new UpdateEmployees(employeeDAO).execute();
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                employeeDAO.updateAll();
            }
        });
    }
//    private static class UpdateEmployees extends AsyncTask<Employee, Void, Void>{
//
//        private EmployeeDAO employeeDAO;
//
//        public UpdateEmployees(EmployeeDAO employeeDAO) {
//            this.employeeDAO = employeeDAO;
//        }
//
//        @Override
//        protected Void doInBackground(Employee... employees) {
//            employeeDAO.updateAll();
//            return null;
//        }
//    }


    /**
     * Delete an employee
     * @param e
     */
    public void deleteEmployee(final Employee e){
//        new DeleteEmployee(employeeDAO).execute(e);
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                employeeDAO.delete(e);
            }
        });
    }
//    private static class DeleteEmployee extends AsyncTask<Employee, Void, Void>{
//
//        private EmployeeDAO employeeDAO;
//
//        public DeleteEmployee(EmployeeDAO employeeDAO) {
//            this.employeeDAO = employeeDAO;
//        }
//
//        @Override
//        protected Void doInBackground(Employee... employees) {
//            employeeDAO.delete(employees[0]);
//            return null;
//        }
//    }

    /**
     * Delete all employees
     */
    public void deleteAllEmployees(){
//        new DeleteEmployees(employeeDAO).execute();
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                employeeDAO.deleteAll();
            }
        });

    }
//    private static class DeleteEmployees extends AsyncTask<Void, Void, Void>{
//
//        private EmployeeDAO employeeDAO;
//
//        public DeleteEmployees(EmployeeDAO employeeDAO) {
//            this.employeeDAO = employeeDAO;
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            employeeDAO.deleteAll();
//            return null;
//        }
//    }

    /**
     * Fetch list of all employees
     * @return
     */
    public LiveData<List<Employee>> getAllRecords() {
        return allRecords;
    }
}
