package com.example.officerecords.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.officerecords.model.Employee;
import com.example.officerecords.model.EmployeeRepository;

import java.util.List;

public class EmployeeViewModel extends AndroidViewModel {

    private EmployeeRepository employeeRepository;
    private LiveData<List<Employee>> allRecords;

    public EmployeeViewModel(@NonNull Application application) {
        super(application);

        employeeRepository = new EmployeeRepository(application);
        allRecords = employeeRepository.getAllRecords();
    }

    public void insertEmployee(Employee e){
        employeeRepository.insertEmployee(e);
    }

    public void updateEmployee(Employee e){
        employeeRepository.updateEmployee(e);
    }

    public void deleteEmployee(Employee e){
        employeeRepository.deleteEmployee(e);
    }

    public void deleteAllEmployees(){
        employeeRepository.deleteAllEmployees();
    }

    public void updateAllEmployees(){
        employeeRepository.updateEmployees();
    }

    public LiveData<List<Employee>> getAllRecords(){
        return allRecords;
    }
}
